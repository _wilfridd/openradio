/*
 * Copyright (C) 2020  Wproject - Aitzol Berasategi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * OpenRadio is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Window 2.2
import QtMultimedia 5.6
import Ubuntu.Components 1.3
import Qt.labs.settings 1.0
import io.thp.pyotherside 1.3
import Custom 1.0 // Custom plugin
import "components"

ApplicationWindow {
    id: root
    visible: true
    title: "OpenRadio"

    width: units.gu(45)
    height: units.gu(75)

    property string headerTitle
    property var version: "1.0.1"
    property real items
    property string defaultIcon: '../images/default.svg'

    property real currentIndex
    property string nowPlaying
    property var selectedStation
    property var favorites
    property var stations
    property var listModel

    property var orientation : width>height ? 2:1
    property real density: Screen.pixelDensity

    property var mainHeader : MainHeader{}
    property var searchHeader: SearchHeader{}
    property var multiPageHeader: MultiPageHeader{}
    property var drawer: Drawer{}
    property var confirmationDialog: ConfirmationDialog{}
    property var sleepDialog: SleepDialog{}
    property var notificationDialog: NotificationDialog{}

    property bool favoritesView: true
    property bool sleep: false
    property var output
    property alias currentHeader: root.header

    property bool deviceType: Custom.isTouch //from C++ Dino plugin

    Settings {
        id: settings
        property string style: "Suru" //not in use
        property string country: settings.firstRun ? Custom.country : settings.country //from C++ Custom plugin
        property string theme: "Ubuntu.Components.Themes.Ambiance"
        property bool deviceType: Custom.isTouch //from C++ Custom plugin
        property bool firstRun: true
        property bool gridView: false
        property bool infoAlert: true
    }
    
    color: theme.palette.normal.background
    header: mainHeader

    StackView {
        id: stackView
        anchors.fill: parent
        pushEnter: Transition {
            XAnimator {
                from: (stackView.mirrored ? -1 : 1) * -stackView.width
                to: 0
                duration: 300
                easing.type: Easing.OutCubic
            }
        }

        popExit: Transition {
            XAnimator {
                from: 0
                to: (stackView.mirrored ? -1 : 1) * stackView.width
                duration: 300
                easing.type: Easing.OutCubic
            }
        }

        Component.onCompleted:{
            //PROVISIONAL
            console.log("touchScreen:" + settings.deviceType)
            console.log(settings.country)
            console.log(settings.firstRun)
            console.log('i: '+settings.infoAlert)
            python.call('app.main.db_conn',[], function(result){
                console.log('db connected!!')
            });

            // Show the welcome wizard only when running the app for the first time
            if (settings.firstRun) {
                console.log("[LOG]: Detecting first time run. Starting welcome wizard.")

                //create favorites table
                python.call('app.main.create_favorites_table',[], function(result){
                    console.log('db favorites created!!')
                });

                //create stations table by country
                python.call('app.main.create_stations_table',[settings.country], function(result){
                    console.log('db stations created!!')
                });

                //PUSH INTRO... instead of PAGE1
                push(page1)
                settings.firstRun = false;
                //push(Qt.resolvedUrl("intro/Intro.qml"))

            } else {
                //get stations from db
                push(page1)
            }

        }

    }

    MainView{
        //Only for making translation work
        id: dummyMainView
        applicationName: "openradio.aitzol76"
        visible: false
    }

    RadioListview {
        id: page1
        theme.name: settings.theme
        SleepTimerRibbon {
            id: ribbon
        }
    }

    StationView {
        id: page2
        theme.name: settings.theme
    }

    SettingsPage{
        id:settingsPage
        theme.name: settings.theme
    }

    AboutPage{
        id:aboutPage
        theme.name: settings.theme
    }

    SearchFieldComponent {
        id: searchFieldComponent
    }

    Audio {
        id: playRadio
        source: ""

        function playChannel(i){
            playRadio.source = listModel[i].url;
            console.log(listModel[i].url)
            playRadio.play();
        }
        
    }

    Python {
        id: python

        Component.onCompleted: {
            addImportPath(Qt.resolvedUrl('../src/'));

            importModule('app', function() {
                console.log('app module imported');
                
                call('app.main.db_conn', [], function(res){
                    //make something with "res" if OK get favorites.....
                });
                
                call('app.main.get_favorites', [], function(res){
                    //do something with "res"
                    favorites = res;
                    listModel = favorites
                });
                call('app.main.get_stations', [], function(res){
                    //do something with "res"
                    stations = res;
                    //listModel = stations
                });
            });

        }

        onError: {
            console.log('python error: ' + traceback);
        }
    }

    Component.onCompleted: Custom.main()

}
