'''
 Copyright (C) 2020  Wproject - Aitzol Berasategi

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3.

 openradio is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
'''

import os
import requests
import sqlite3
import uuid

class OpenRadio(object):

	def __init__(self):
		self.debug = ''
		self.result = []
		self.dbDir = os.environ['HOME'] + '/.local/share/openradio.aitzol76/Databases/'
		#self.dbDir = '/home/phablet/.local/share/openradio.aitzol76/Databases/'
		self.imgDir = os.environ['HOME'] + '/.local/share/openradio.aitzol76/Images/'
		#self.imgDir = '/home/phablet/.local/share/openradio.aitzol76/Images/'
		self.defaultImage = '../images/default.svg'

	def db_conn(self):

		if not os.path.exists(self.dbDir):
			os.makedirs(self.dbDir)

		# database connection
		try:
			self.dbConnection = sqlite3.connect(self.dbDir + 'openradioDB')
			self.dbCursor = self.dbConnection.cursor()
			self.debug = "OK"

		except sqlite3.Error as e:
			self.debug = e.args[0]

	def create_stations_table(self,country):

		self.country = country;
		self.stations = self.get_stations_by_country(self.country);

		# database connection
		try:
			#self.dbConnection = sqlite3.connect("/home/phablet/.local/share/macbank.aitzol76/Databases/a800b0b231b400980e8fef526760d58d") //testing
			#self.dbConnection = sqlite3.connect(self.dbDir + 'openradioDB')
			#self.dbCursor = self.dbConnection.cursor()
			self.dbCursor.execute("CREATE TABLE IF NOT EXISTS stationstable (name TEXT, url TEXT, favicon TEXT, homepage TEXT, fav INT DEFAULT 0)")
			self.debug = "OK"

			try:
				self.dbCursor.executemany("INSERT OR REPLACE INTO stationstable (name, url, favicon, homepage) VALUES(?,?,?,?)", (self.stations))
				self.dbConnection.commit()

			except sqlite3.Error as e:
				self.debug = e.args[0]

		except sqlite3.Error as e:
			self.debug = e.args[0]

		finally:
			return self.debug

	def change_country(self, country):
		self.country = country;
		try:
			drop = "DROP TABLE stationstable"
			self.dbCursor.execute(drop)
			self.create_stations_table(self.country)
		except sqlite3.Error as e:
			self.debug = e.args[0]

	def get_stations_by_country(self,country):

		#self.country = 'spain'
		self.country = country
		#self.url = 'https://de1.api.radio-browser.info/json/stations/bycountry/'+self.country+'?hidebroken=true'
		self.url = 'https://de1.api.radio-browser.info/json/stations/bycountrycodeexact/'+self.country+'?hidebroken=true'
		#self.url = 'https://de1.api.radio-browser.info/json/stations/bycountrycodeexact/GB?hidebroken=true'

		try:
			r = requests.get(self.url, headers={})
			res = r.json()
			stations = []
			
			for i in res:
				st = []
				st.append(i['name'])
				st.append(i['url'])
				st.append(i['favicon'])
				st.append(i['homepage'])
				stations.append(tuple(st))

			return stations

		except requests.exceptions.HTTPError as e:
			print('http error: ', e)
		else:
			self.timeline = r.json()
			print('Timeline length: ')
			return ('time_line')


	def create_favorites_table(self):

		if not os.path.exists(self.dbDir):
			os.makedirs(self.dbDir)
		#self.db_conn()

		defaultFavorites = [("Fox News Radio", "https://streaming-ent.shoutcast.com/foxnews", "../images/foxnewsradio.png", "FOX News Talk Web Stream."),
			("100% NL", "http://stream.100p.nl/100pctnl.mp3", "../images/100nl.png", "http://www.100p.nl"),
			("Antenne Bayern", "http://mp3channels.webradio.antenne.de/antenne", "../images/anttene_bayern.png", "Antenne Bayern ist ein privater Hörfunksender mit Sitz in Ismaning, Bayern."),
			("BBC World Service", "http://bbcwssc.ic.llnwd.net/stream/bbcwssc_mp1_ws-einws", "../images/bbcws.png", "http://www.bbc.co.uk/worldserviceradio" ),
			("Cadena Ser", "http://20073.live.streamtheworld.com:80/CADENASER.mp3", "../images/cadenaser.png", "La cadena de radio más antigua de España y la que cuenta con más oyentes según el EGM."),
			("Radio Euskadi", "http://mp3-eitb.stream.flumotion.com/eitb/radioeuskadi.mp3", "../images/reuskadi.jpg", "Radio Euskadi - Eitb"),
			("Hits Radio", "https://stream-al.planetradio.co.uk/hits.aac", "../images/hitsradio.png", "https://planetradio.co.uk/hits-radio/"),
			("RTL", "http://streaming.radio.rtl.fr/rtl-1-44-128", "../images/rtl.png", "https://www.rtl.fr/"),
			("Catalunya Radio", "http://www.catradio.cat/directes/catradio_http.m3u", "../images/catradio.png", "http://www.catradio.cat/"),]
		# database connection
		try:
			#self.dbConnection = sqlite3.connect("/home/phablet/.local/share/macbank.aitzol76/Databases/a800b0b231b400980e8fef526760d58d") //testing
			#self.dbConnection = sqlite3.connect(self.dbDir + 'openradioDB')
			#self.dbCursor = self.dbConnection.cursor()
			self.dbCursor.execute("CREATE TABLE IF NOT EXISTS favoritestable (name TEXT, url TEXT, favicon TEXT, homepage TEXT)")
			self.debug = "OK"

			try:
				self.dbCursor.executemany("INSERT OR REPLACE INTO favoritestable VALUES(?,?,?,?)", defaultFavorites)
				self.dbConnection.commit()

			except sqlite3.Error as e:
				self.debug = e.args[0]

		except sqlite3.Error as e:
			self.debug = e.args[0]

		finally:
			return self.debug


	def get_favorites(self):
		#self.db_conn()

		self.dbCursor.execute("SELECT * FROM favoritestable")
		self.rows = self.dbCursor.fetchall()
		self.result = []
		if self.rows:
			for row in self.rows:
				self.fav = {}
				self.fav['name'] = row[0]
				self.fav['url'] = row[1]
				self.fav['favicon'] = row[2]
				self.fav['homepage'] = row[3]
				self.result.append(self.fav)

			return self.result
		else:
			return ('empty')

	def get_stations(self):
		#self.db_conn()
		self.dbCursor.execute("SELECT * FROM stationstable WHERE fav = 0")
		self.rows = self.dbCursor.fetchall()
		self.result = []
		if self.rows:
			for row in self.rows:
				self.fav = {}
				self.fav['name'] = row[0]
				self.fav['url'] = row[1]
				self.fav['favicon'] = row[2]
				self.fav['homepage'] = row[3]
				self.result.append(self.fav)

			return self.result
		else:
			return ('empty')


	def add_to_favorites(self, name, url, favicon, homepage):

		try:
			# create table if it doesnt exist
			self.dbCursor.execute("CREATE TABLE IF NOT EXISTS favoritestable (name TEXT, url TEXT, favicon TEXT, homepage TEXT)")
			self.debug = 'add favorite: step 1'
			try:
				# add station to favorites table
				self.dbCursor.executemany("INSERT OR REPLACE INTO favoritestable VALUES(?,?,?,?)", [(name, url, self.img_download(favicon), homepage)])
				self.dbConnection.commit()
				self.debug = 'add favorite: step 2'
				try:
					# mark station as favorite
					self.dbCursor.executemany("UPDATE stationstable SET fav = ? WHERE name = ?", [(1, name)])
					self.dbConnection.commit()
					self.debug = 'add favorite: Success!'
				except sqlite3.Error as e:
					self.debug = 'error:'+e.args[0]
			except sqlite3.Error as e:
				self.debug = e.args[0]
		except sqlite3.Error as e:
			self.debug = e.args[0]
		return self.debug


	def remove_favorite(self, name):
		try:
			self.dbCursor.execute("DELETE FROM favoritestable WHERE name = ?", [(name)])
			self.dbConnection.commit()
			try:
				# mark station as favorite
				self.dbCursor.executemany("UPDATE stationstable SET fav = ? WHERE name = ?", [(0, name)])
				self.dbConnection.commit()
				self.debug = 'removed from favorites: Success!'
				return self.get_stations()
			except sqlite3.Error as e:
				self.debug = 'error:'+e.args[0]
		except sqlite3.Error as e:
			self.debug = e.args[0]

	def img_download(self, image_url):

		if not os.path.exists(self.imgDir):
			os.makedirs(self.imgDir)

		try:
			filename = uuid.uuid4().hex;
			path = self.imgDir+ filename + '.' + image_url.split('.')[-1]
			r = requests.get(image_url, allow_redirects=True)
			if(r.status_code == 200):
				open(path, 'wb').write(r.content)
				'''
				with open(path, 'wb') as f:
				    f.write(r.content)
				    f.close()
				'''
			else:
				path = self.defaultImage
		except:
			path = self.defaultImage
			
		return path

main=OpenRadio()
